package com.pradip.spring.oauth.springoauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWithOauthApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringWithOauthApplication.class, args);
	}

}
